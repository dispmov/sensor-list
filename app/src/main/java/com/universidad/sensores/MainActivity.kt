package com.universidad.sensores

import android.content.Context
import android.hardware.Sensor
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.hardware.SensorManager
import android.widget.ListView
import android.widget.TextView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import java.lang.reflect.Array

class MainActivity : AppCompatActivity() {

    private lateinit var sensorManager: SensorManager

    public lateinit var mTitle: TextView
    public lateinit var mSensors: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mTitle = findViewById(R.id.tTitle) as TextView
        mSensors = findViewById(R.id.lvSensorList) as ListView

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val deviceSensors: List<Sensor> = sensorManager.getSensorList(Sensor.TYPE_ALL)


        mTitle.setText("Lista de sensores")
        val Sadapter = SensorAdapter(this, deviceSensors)
        mSensors.adapter = Sadapter
    }



    private  class  SensorAdapter(private val context: Context, private val dataSource: List<Sensor>): BaseAdapter(){

        private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        override fun getItem(position: Int): Any {
            return dataSource[position]
        }
        override fun getItemId(position: Int): Long {
            return position.toLong()
        }
        override fun getCount(): Int {
            return dataSource.size
        }
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val rowView = inflater.inflate(R.layout.item_list_view, parent, false)
            val Itemtitle = rowView.findViewById(R.id.tIIemTitle) as TextView
            val ItemDescription = rowView.findViewById(R.id.tIIemDescription) as TextView
            val sensor = getItem(position) as Sensor
            Itemtitle.text = sensor.name
            ItemDescription.text = sensor.vendor

            return rowView
        }
    }
}
